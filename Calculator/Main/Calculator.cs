﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
   // 计算器类
   class Calculator
   {
      // 计算器UI模块
      public UI Ui { get; set; }

      // 计算器计算核心模块
      public Core Core { get; set; }

      public Calculator(MainWindow window)
      {
         Ui = new UI(window);
         Core = new Core();
      }

      // 计算
      public string GetResult()
      {
         if (Ui.IsError())
            throw new Exception();

         string tarExp = Ui.ExpTxt;

         try
         {
            return Core.CalcExpCore(tarExp).ToString();
         }
         catch
         {
            throw;
         }
      }
   }
}
