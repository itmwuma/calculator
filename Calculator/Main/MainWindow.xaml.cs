﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Main
{
   /// <summary>
   /// MainWindow.xaml 的交互逻辑
   /// </summary>
   public partial class MainWindow : Window
   {
      // 计算器
      private Calculator calculator;

      public MainWindow()
      {
         // 初始化对象
         InitializeComponent();
         calculator = new Calculator(this);
         calculator.Ui.ExpTxt = "0";
      }

      // 操作数按钮点击操作
      private void NumBtnClicked(object sender, RoutedEventArgs e)
      {
         Button btn = sender as Button;
         calculator.Ui.OpBtnUpdate(btn);
      }

      // 回退键按钮点击操作
      private void BackBtnClicked(object sender, RoutedEventArgs e)
      {
         calculator.Ui.BackBtnUpdate();
      }

      // 请空键按钮点击操作
      private void ClearBtnClicked(object sender, RoutedEventArgs e)
      {
         calculator.Ui.ClearBtnUpdate();
      }

      // 操作符按钮点击操作
      private void OpBtnClicked(object sender, RoutedEventArgs e)
      {
         Button btn = sender as Button;
         calculator.Ui.OpBtnUpdate(btn);
      }

      // 求值按钮点击操作
      private void EqualBtnClicked(object sender, RoutedEventArgs e)
      {
         try
         {
            calculator.Ui.ExpTxt = calculator.GetResult();
         }
         catch
         {
            calculator.Ui.PrintError();
         }
      }
   }
}
