﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Main
{
   class UI
   {
      // 主界面
      private MainWindow Window { get; set; }

      public UI(MainWindow window)
      {
         Window = window;
      }

      // 文本框文本信息
      public string ExpTxt
      {
         get { return Window.txtBox.Text; }
         set { Window.txtBox.Text = value; }
      }

      // 操作按钮更新文本信息
      public void OpBtnUpdate(Button btn)
      {
         // 文本框显示信息
         string content = btn.Content.ToString();

         // 清空初始值
         if ((ExpTxt == "0" && content != ".") || IsError())
            ExpTxt = "";

         ExpTxt += content;
      }

      // 回退键更新文本信息
      public void BackBtnUpdate()
      {
         if (ExpTxt == "0")
            return;

         // Error错误提示全部清空
         if (IsError())
         {
            ClearBtnUpdate();
            return;
         }

         ExpTxt = ExpTxt.Remove(ExpTxt.Length - 1);
         if (ExpTxt.Length == 0)
            ExpTxt = "0";
      }

      // 清空键更新文本信息
      public void ClearBtnUpdate()
      {
         ExpTxt = "0";
      }

      // 打印错误
      public void PrintError()
      {
         ExpTxt = "ERROR";
      }

      // 判断是否错误
      public bool IsError()
      {
         return ExpTxt == "ERROR";
      }
   }
}
