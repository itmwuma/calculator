# 简易计算器

#### 介绍
简易计算器WPF程序，包括加、减、乘、除、括号等计算操作

实现了简单表达式计算、表达式判断等功能



#### 软件架构
- **开发框架**：WPF（.Net Framework 4.7.2）
- **开发语言**：C#



#### 项目架构

<img src="img/UML.png" alt="UML类图" style="zoom:80%;" />



#### 使用说明

1. 简单表达式计算
<img src="img/img01.png" style="zoom: 67%;" />

<img src="img/img03.png" style="zoom: 67%;" />

2. 复杂表达式计算

<img src="img/img02.png" style="zoom: 67%;" />

<img src="img/img04.png" style="zoom: 67%;" />

3. 表达式检查

<img src="./img/img05.png" style="zoom: 67%;" />

<img src="./img/img06.png" style="zoom: 67%;" />



#### 参与贡献
- 作者：itmWuma
- gitee项目首页：https://gitee.com/itmwuma/calculator
